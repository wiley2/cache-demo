package com.wiley.cachedemo.cache.inmemory;

import com.wiley.cachedemo.cache.EmployeeCache;
import com.wiley.cachedemo.cache.enums.CacheLevel;
import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.config.CacheConfig;
import com.wiley.cachedemo.config.CacheConfigsLoader;
import com.wiley.cachedemo.model.Employee;
import com.wiley.cachedemo.repository.EmployeeRepository;
import com.wiley.cachedemo.service.EmployeeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class LFUCacheTest {

    private static final int CACHE_SIZE = 3;
    private final EmployeeCache employeeCache = EmployeeCache.invalidateAndReInitiate();
    @Mock
    public EmployeeRepository employeeRepository;
    @InjectMocks
    private EmployeeService employeeService;

    @BeforeAll
    public static void init() {
        final CacheConfig inMemoryCacheConfigs = CacheConfigsLoader.inMemoryCacheConfigs();
        inMemoryCacheConfigs.getEvictionStrategyConfig().setUseForce(true);
        inMemoryCacheConfigs.getEvictionStrategyConfig().setValue(EvictionStrategy.LFU);
        inMemoryCacheConfigs.getMaxSizeConfig().setValue(CACHE_SIZE);
        inMemoryCacheConfigs.getMaxSizeConfig().setUseForce(true);
    }

    private void performDataRetrieval() {
        employeeCache.clear();
        when(employeeRepository.getOne(1L)).thenReturn(new Employee(1, "F1", "F2", 5));
        when(employeeRepository.getOne(2L)).thenReturn(new Employee(2, "F1", "F2", 5));
        when(employeeRepository.getOne(3L)).thenReturn(new Employee(3, "F1", "F2", 5));
        when(employeeRepository.getOne(4L)).thenReturn(new Employee(4, "F1", "F2", 5));
        when(employeeRepository.getOne(5L)).thenReturn(new Employee(5, "F1", "F2", 5));

        employeeService.getById(2L);
        employeeService.getById(1L);
        employeeService.getById(3L);
        employeeService.getById(2L);
        employeeService.getById(1L);
        employeeService.getById(2L);
        employeeService.getById(4L);
        employeeService.getById(1L);
        employeeService.getById(4L);
        employeeService.getById(4L);
        employeeService.getById(5L);
    }

    /**
     * Testing single level caching add and put with LFU eviction strategy
     */
    @Test
    public void testLFUCache_addAndPut() {
        performDataRetrieval();

        assertEquals(CACHE_SIZE, employeeCache.sizeByLevel(CacheLevel.LEVEL_ONE));
        List<Employee> employees = employeeCache.getAllDataByLevel(CacheLevel.LEVEL_ONE);

        assertEquals(1L, employees.get(0).getId());
        assertEquals(4L, employees.get(1).getId());
        assertEquals(5L, employees.get(2).getId());
    }

    @Test
    public void testLFUCache_remove() {
        performDataRetrieval();

        employeeCache.remove(4L);

        assertEquals(CACHE_SIZE - 1, employeeCache.sizeByLevel(CacheLevel.LEVEL_ONE));
        List<Employee> employeesAfterRemoval = employeeCache.getAllDataByLevel(CacheLevel.LEVEL_ONE);

        assertEquals(1L, employeesAfterRemoval.get(0).getId());
        assertEquals(5L, employeesAfterRemoval.get(1).getId());
    }
}
