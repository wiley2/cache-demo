package com.wiley.cachedemo.cache.filesystem;

import com.wiley.cachedemo.cache.EmployeeCache;
import com.wiley.cachedemo.cache.enums.CacheLevel;
import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.config.CacheConfig;
import com.wiley.cachedemo.config.CacheConfigsLoader;
import com.wiley.cachedemo.model.Employee;
import com.wiley.cachedemo.repository.EmployeeRepository;
import com.wiley.cachedemo.service.EmployeeService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class LFUCacheTest {

    private static final int IN_MEMORY_CACHE_SIZE = 3;
    private static final int FILE_SYSTEM_CACHE_SIZE = 5;
    private final EmployeeCache employeeCache = EmployeeCache.invalidateAndReInitiate();
    @Mock
    public EmployeeRepository employeeRepository;
    @InjectMocks
    private EmployeeService employeeService;

    {
        employeeCache.clear();
    }

    @BeforeAll
    public static void init() {
        final CacheConfig inMemoryCacheConfigs = CacheConfigsLoader.inMemoryCacheConfigs();
        inMemoryCacheConfigs.getEvictionStrategyConfig().setUseForce(true);
        inMemoryCacheConfigs.getEvictionStrategyConfig().setValue(EvictionStrategy.LFU);
        inMemoryCacheConfigs.getMaxSizeConfig().setValue(IN_MEMORY_CACHE_SIZE);
        inMemoryCacheConfigs.getMaxSizeConfig().setUseForce(true);

        final CacheConfig fsCacheConfigs = CacheConfigsLoader.fileSystemCacheConfigs();
        fsCacheConfigs.getEvictionStrategyConfig().setUseForce(true);
        fsCacheConfigs.getEvictionStrategyConfig().setValue(EvictionStrategy.LFU);
        fsCacheConfigs.getMaxSizeConfig().setValue(FILE_SYSTEM_CACHE_SIZE);
        fsCacheConfigs.getMaxSizeConfig().setUseForce(true);
    }

    /**
     * Testing caching and LFU eviction strategy for both two levels
     */
    @Test
    public void testLFUCache_multiLevels() {
        when(employeeRepository.getOne(1L)).thenReturn(new Employee(1, "F1", "F2", 5));
        when(employeeRepository.getOne(2L)).thenReturn(new Employee(2, "F1", "F2", 5));
        when(employeeRepository.getOne(3L)).thenReturn(new Employee(3, "F1", "F2", 5));
        when(employeeRepository.getOne(4L)).thenReturn(new Employee(4, "F1", "F2", 5));
        when(employeeRepository.getOne(5L)).thenReturn(new Employee(5, "F1", "F2", 5));
        when(employeeRepository.getOne(6L)).thenReturn(new Employee(6, "F1", "F2", 5));

        employeeService.getById(1L);
        employeeService.getById(2L);
        employeeService.getById(3L);
        employeeService.getById(4L);
        employeeService.getById(1L);
        employeeService.getById(5L);
        employeeService.getById(6L);

        // Level 1
        assertEquals(IN_MEMORY_CACHE_SIZE, employeeCache.sizeByLevel(CacheLevel.LEVEL_ONE));
        List<Employee> inMemoryEmployees = employeeCache.getAllDataByLevel(CacheLevel.LEVEL_ONE);

        assertEquals(4L, inMemoryEmployees.get(0).getId());
        assertEquals(5L, inMemoryEmployees.get(1).getId());
        assertEquals(6L, inMemoryEmployees.get(2).getId());

        // Level 2
        assertEquals(FILE_SYSTEM_CACHE_SIZE, employeeCache.sizeByLevel(CacheLevel.LEVEL_TWO));
        List<Employee> fileSystemEmployees = employeeCache.getAllDataByLevel(CacheLevel.LEVEL_TWO);

        assertEquals(1L, fileSystemEmployees.get(0).getId());
        assertEquals(3L, fileSystemEmployees.get(1).getId());
        assertEquals(4L, fileSystemEmployees.get(2).getId());
        assertEquals(5L, fileSystemEmployees.get(3).getId());
        assertEquals(6L, fileSystemEmployees.get(4).getId());
    }
}
