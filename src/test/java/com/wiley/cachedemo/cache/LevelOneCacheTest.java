package com.wiley.cachedemo.cache;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.config.CacheConfig;
import com.wiley.cachedemo.config.CacheConfigsLoader;
import com.wiley.cachedemo.model.Product;
import com.wiley.cachedemo.repository.ProductRepository;
import com.wiley.cachedemo.service.ProductService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ActiveProfiles("test")
@SpringBootTest
public class LevelOneCacheTest {

    private static final int CACHE_SIZE = 3;
    private final ProductCache productCache = ProductCache.getInstance();
    @Mock
    public ProductRepository productRepository;
    @InjectMocks
    private ProductService productService;

    @BeforeAll
    public static void init() {
        final CacheConfig inMemoryCacheConfigs = CacheConfigsLoader.inMemoryCacheConfigs();
        inMemoryCacheConfigs.getEvictionStrategyConfig().setUseForce(true);
        inMemoryCacheConfigs.getEvictionStrategyConfig().setValue(EvictionStrategy.LFU);
        inMemoryCacheConfigs.getMaxSizeConfig().setValue(CACHE_SIZE);
        inMemoryCacheConfigs.getMaxSizeConfig().setUseForce(true);
    }

    private void performDataRetrieval() {
        productCache.clear();
        when(productRepository.getOne(1L)).thenReturn(new Product(1, "P1"));
        when(productRepository.getOne(2L)).thenReturn(new Product(2, "P1"));
        when(productRepository.getOne(3L)).thenReturn(new Product(3, "P1"));
        when(productRepository.getOne(4L)).thenReturn(new Product(4, "P1"));
        when(productRepository.getOne(5L)).thenReturn(new Product(5, "P1"));

        productService.getById(2L);
        productService.getById(1L);
        productService.getById(3L);
        productService.getById(2L);
        productService.getById(1L);
        productService.getById(2L);
        productService.getById(4L);
        productService.getById(1L);
        productService.getById(4L);
        productService.getById(4L);
        productService.getById(5L);
    }

    /**
     * Testing single level caching add and put with LFU eviction strategy
     */
    @Test
    public void testLFUCache_addAndPut() {
        performDataRetrieval();

        assertEquals(CACHE_SIZE, productCache.size());
        List<Product> products = productCache.getAllData();

        assertEquals(1L, products.get(0).getId());
        assertEquals(4L, products.get(1).getId());
        assertEquals(5L, products.get(2).getId());
    }

    /**
     * Testing single level cache remove feature
     */
    @Test
    public void testLFUCache_remove() {
        performDataRetrieval();

        productCache.remove(4L);

        assertEquals(CACHE_SIZE - 1, productCache.size());
        List<Product> productsAfterRemoval = productCache.getAllData();

        assertEquals(1L, productsAfterRemoval.get(0).getId());
        assertEquals(5L, productsAfterRemoval.get(1).getId());
    }
}
