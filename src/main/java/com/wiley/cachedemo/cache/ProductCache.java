package com.wiley.cachedemo.cache;

import com.wiley.cachedemo.cache.enums.CacheType;
import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.model.Product;

/**
 * Cache to store {@link Product} objects by it's ID
 */
public class ProductCache extends SingleLevelCache<Long, Product> {

    private static final ProductCache instance = new ProductCache();

    private ProductCache() {
        super(3, EvictionStrategy.LFU, CacheType.IN_MEMORY);
    }

    public static ProductCache getInstance() {
        return instance;
    }

    @Override
    public Long getKey(Product product) {
        return product.getId();
    }
}
