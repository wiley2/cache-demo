package com.wiley.cachedemo.cache;

import com.wiley.cachedemo.cache.enums.CacheLevel;
import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.cache.filesystem.FileSystemCache;
import com.wiley.cachedemo.cache.inmemory.InMemoryCache;
import com.wiley.cachedemo.cache.setup.Cache;
import com.wiley.cachedemo.cache.setup.LevelBasedCache;

import java.io.Serializable;
import java.util.List;

/**
 * Abstract class for multi level caches
 * Note that the constructor of the class is currently implemented only for two levels
 *
 * @param <K> Key type
 * @param <V> value type
 */
public abstract class MultiLevelCache<K, V extends Serializable> implements Cache<K, V> {

    private final LevelBasedCache<K, V> cache;

    /**
     * Constructor of multi level cache class
     * Currently it support only for two levels
     * But changing constructor can make it available for multiple levels
     */
    protected MultiLevelCache(int level1MaxSize, EvictionStrategy level1EvictionStrategy, int level2MaxSize, EvictionStrategy level2EvictionStrategy) {
        // Creating level 1 cache
        cache = new InMemoryCache<>(level1MaxSize, level1EvictionStrategy);
        cache.setLevel(CacheLevel.LEVEL_ONE);

        // Creating level 2 cache
        LevelBasedCache<K, V> level2Cache = new FileSystemCache<>(level2MaxSize, level2EvictionStrategy, this);
        level2Cache.setLevel(CacheLevel.LEVEL_TWO);

        cache.setNextLevelCache(level2Cache);
        level2Cache.setPreviousLevelCache(cache);
    }

    @Override
    public V get(K key) {
        return cache.getByLevel(key);
    }

    @Override
    public void put(V v) {
        cache.putByLevel(getKey(v), v);
    }

    @Override
    public void clear() {
        cache.clearByLevel();
    }

    @Override
    public void remove(K key) {
        cache.removeByLevel(key);
    }

    public int sizeByLevel(CacheLevel level) {
        return cache.sizeByLevel(level);
    }

    @Override
    public int size() {
        return 0;
    }

    public List<V> getAllDataByLevel(CacheLevel level) {
        return cache.getAllDataByLevel(level);
    }

    @Override
    public List<V> getAllData() {
        return null;
    }
}
