package com.wiley.cachedemo.cache.filesystem;

import com.wiley.cachedemo.cache.setup.BaseCache;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * File system cache implementation with eviction strategy of LRU
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class LRUCache<K, V> extends FSCache implements BaseCache<K, V> {

    private final int maxSize;

    public LRUCache(String cacheStorageKey, int maxSize) {
        super(cacheStorageKey);
        this.maxSize = maxSize;
    }

    @Override
    public V get(Object key) {
        File cacheElement = findCacheElement(key);

        if (cacheElement == null) {
            return null;
        }

        // set last modified date as current time
        // so that it can be identified easily that this file has been used recently
        cacheElement.setLastModified(System.currentTimeMillis());

        return readFile(cacheElement);
    }

    @Override
    public V put(K key, V v) {
        File cacheElement = new File(getCacheDir(), String.valueOf(key));

        if (size() >= maxSize) {
            evictElement();
        }

        writeFile(cacheElement, v);
        return v;
    }

    private void evictElement() {
        final File[] files = getCacheDir().listFiles();

        if (files == null) {
            return;
        }

        File eldestFile = null;
        long eldestTime = Long.MAX_VALUE;

        // finding oldest file in the cache by checking last modified time
        for (File file : files) {
            if (file.lastModified() < eldestTime) {
                eldestTime = file.lastModified();
                eldestFile = file;
            }
        }

        if (eldestFile != null) {
            deleteFile(eldestFile);
        }
    }

    @Override
    public V remove(K key) {
        File cacheElement = findCacheElement(key);
        if (cacheElement != null) {
            deleteFile(cacheElement);
        }
        return null;
    }

    @Override
    public void clear() {
        final File[] files = getCacheDir().listFiles();

        if (files == null) {
            return;
        }

        for (File file : files) {
            deleteFile(file);
        }
    }

    @Override
    public int size() {
        final String[] list = getCacheDir().list();
        return list == null ? 0 : list.length;
    }

    @Override
    public List<V> getAllData() {
        final File[] files = getCacheDir().listFiles();

        List<V> allData = new ArrayList<>();
        if (files == null) {
            return allData;
        }

        for (File file : files) {
            allData.add(readFile(file));
        }

        return allData;
    }
}
