package com.wiley.cachedemo.cache.filesystem;

import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper class for file system cache
 */
public class FSCache {

    private final String cacheStorageKey;

    /**
     * Cache folder
     */
    private final File cacheDir;

    private final Logger logger = Logger.getLogger("FILE SYSTEM");

    public FSCache(String cacheStorageKey) {
        this.cacheStorageKey = cacheStorageKey;
        this.cacheDir = resolveCacheDir();
    }

    protected File getCacheDir() {
        return cacheDir;
    }

    /**
     * Find cache file by given key
     *
     * @param key Key to find
     * @return Cache file
     */
    protected File findCacheElement(Object key) {
        File cacheElement = new File(getCacheDir(), String.valueOf(key));

        if (cacheElement.exists()) {
            return cacheElement;
        }

        return null;
    }

    /**
     * Create and validate cache storage location and return
     *
     * @return Cache storage location
     */
    protected File resolveCacheDir() {

        // location of classpath of project
        ClassPathResource classpath = new ClassPathResource("");

        File cacheLocation;
        try {
            cacheLocation = new File(classpath.getFile(), "caches");
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, e.getMessage());
            return null;
        }

        // create cache folder, if not exists
        File cacheDir = new File(cacheLocation, cacheStorageKey);

        if (cacheDir.exists()) {
            return cacheDir;
        }

        if (cacheDir.mkdirs()) {
            return cacheDir;
        }

        return null;
    }

    /**
     * Read object stored in a given file
     *
     * @param cacheElement Given file
     * @param <T>          Returning object type
     * @return Generated object
     */
    protected <T> T readFile(File cacheElement) {

        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            fileInputStream = new FileInputStream(cacheElement);
            objectInputStream = new ObjectInputStream(fileInputStream);
            return (T) objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, e.getMessage());
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
            }

            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
            }
        }

        return null;
    }

    /**
     * Write object as a file
     *
     * @param file File to write
     * @param t    Given object
     * @param <T>  Given object type
     */
    protected <T> void writeFile(File file, T t) {
        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;

        try {
            if (!file.exists() && !file.createNewFile()) {
                return;
            }

            fileOutputStream = new FileOutputStream(file);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(t);
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, e.getMessage());
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
            }

            try {
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
            }
        }
    }

    /**
     * Delete a given file
     *
     * @param cacheElement File to be deleted
     */
    protected void deleteFile(File cacheElement) {
        if (cacheElement != null) {
            cacheElement.delete();
        }
    }
}
