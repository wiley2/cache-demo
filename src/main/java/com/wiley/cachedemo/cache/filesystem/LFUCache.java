package com.wiley.cachedemo.cache.filesystem;

import com.wiley.cachedemo.cache.setup.BaseCache;

import java.io.File;
import java.util.*;

/**
 * File system cache implementation with eviction strategy of LFU
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class LFUCache<K, V> extends FSCache implements BaseCache<K, V> {

    /**
     * Max cache size
     */
    private final int maxSize;
    private final File cacheDir, evictionQueueFile, frequencyMapFile;

    public LFUCache(String cacheStorageKey, int maxSize) {
        super(cacheStorageKey);
        this.maxSize = maxSize;
        this.cacheDir = resolveCacheFilesDir();
        this.evictionQueueFile = new File(super.getCacheDir(), "evictionQueue");
        this.frequencyMapFile = new File(super.getCacheDir(), "frequencyMap");
    }

    /**
     * Generate frequency map from saved file
     *
     * @return Generated map
     */
    private Map<Object, Integer> getFrequencyMap() {
        if (!frequencyMapFile.exists()) {
            return new HashMap<>();
        }

        final HashMap<Object, Integer> frequencyMap = readFile(frequencyMapFile);
        if (frequencyMap != null) {
            return frequencyMap;
        }

        return new HashMap<>();
    }

    /**
     * Generate eviction queue from saved file
     *
     * @return Generated queue
     */
    private Queue<Object> getEvictionQueue() {
        if (!evictionQueueFile.exists()) {
            return new ArrayDeque<>();
        }

        ArrayDeque<Object> evictionQueue = readFile(evictionQueueFile);
        if (evictionQueue != null) {
            return evictionQueue;
        }

        return new ArrayDeque<>();
    }

    /**
     * Create and validate the folder which contains cache files
     *
     * @return Folder of cache files
     */
    protected File resolveCacheFilesDir() {
        File baseCacheDir = super.resolveCacheDir();

        if (baseCacheDir == null) {
            return null;
        }

        File cacheDir = new File(baseCacheDir, "cache");
        if (cacheDir.exists()) {
            return cacheDir;
        }

        return cacheDir.mkdir() ? cacheDir : null;
    }

    @Override
    protected File getCacheDir() {
        return this.cacheDir;
    }

    @Override
    public V get(Object key) {
        final File cacheElement = findCacheElement(key);

        if (cacheElement == null) {
            return null;
        }

        // if the object is present in cache, need to update eviction queue
        // retrieving saved eviction queue file and update the value
        final Queue<Object> evictionQueue = getEvictionQueue();
        evictionQueue.add(key);
        // saving updated queue
        writeFile(this.evictionQueueFile, evictionQueue);

        // same thing is done to frequency map too
        final Map<Object, Integer> frequencyMap = getFrequencyMap();
        frequencyMap.merge(key, 1, Integer::sum);
        writeFile(this.frequencyMapFile, frequencyMap);
        return readFile(cacheElement);
    }

    @Override
    public V put(K key, V v) {
        File cacheElement = new File(getCacheDir(), String.valueOf(key));

        // need to evict an element, if the size reached to the max value
        if (size() >= maxSize) {
            evictElement();
        }

        // need to update eviction queue and frequency map
        final Queue<Object> evictionQueue = getEvictionQueue();
        evictionQueue.add(key);
        writeFile(this.evictionQueueFile, evictionQueue);

        final Map<Object, Integer> frequencyMap = getFrequencyMap();
        Integer frequency = frequencyMap.get(key);
        if (frequency != null) {
            frequencyMap.put(key, frequency);
        } else {
            frequencyMap.put(key, 1);
        }
        writeFile(this.frequencyMapFile, frequencyMap);

        writeFile(cacheElement, v);
        return v;
    }

    private void evictElement() {
        int minFreq = Integer.MAX_VALUE;
        final List<Object> keys = new ArrayList<>();

        // finding all keys with minimum frequencies
        final Map<Object, Integer> frequencyMap = getFrequencyMap();
        for (Map.Entry<Object, Integer> frequencyEntry : frequencyMap.entrySet()) {
            if (frequencyEntry.getValue() < minFreq) {
                minFreq = frequencyEntry.getValue();
                keys.clear();
                keys.add(frequencyEntry.getKey());
            } else if (frequencyEntry.getValue() == minFreq) {
                keys.add(frequencyEntry.getKey());
            }
        }

        // determining the key to evict
        final Queue<Object> evictionQueue = getEvictionQueue();
        final Object keyToEvict;
        if (keys.size() == 1) {
            keyToEvict = keys.get(0);
        } else {
            keyToEvict = evictionQueue.stream().filter(keys::contains).findFirst().orElse(null);
        }

        // removing the key from storage and from all other relevant places
        deleteFile(findCacheElement(keyToEvict));

        evictionQueue.remove(keyToEvict);
        frequencyMap.remove(keyToEvict);

        writeFile(this.evictionQueueFile, evictionQueue);
        writeFile(this.frequencyMapFile, frequencyMap);
    }

    @Override
    public V remove(K key) {
        File cacheElement = findCacheElement(key);
        if (cacheElement != null) {
            deleteFile(cacheElement);
        }

        // remove all present elements of the given key in eviction queue
        final Queue<Object> evictionQueue = getEvictionQueue();
        evictionQueue.removeIf(o -> o.equals(key));
        writeFile(this.evictionQueueFile, evictionQueue);

        // remove relevant entry from frequency map too
        final Map<Object, Integer> frequencyMap = getFrequencyMap();
        frequencyMap.remove(key);
        writeFile(this.frequencyMapFile, frequencyMap);

        return null;
    }

    @Override
    public void clear() {
        final File[] files = getCacheDir().listFiles();

        if (files == null) {
            return;
        }

        // clear cache by deleting all files
        for (File file : files) {
            deleteFile(file);
        }

        if (evictionQueueFile.exists()) {
            deleteFile(evictionQueueFile);
        }

        if (frequencyMapFile.exists()) {
            deleteFile(frequencyMapFile);
        }
    }

    @Override
    public int size() {
        final String[] list = getCacheDir().list();
        return list == null ? 0 : list.length;
    }

    @Override
    public List<V> getAllData() {
        final File[] files = getCacheDir().listFiles();

        List<V> allData = new ArrayList<>();
        if (files == null) {
            return allData;
        }

        for (File file : files) {
            allData.add(readFile(file));
        }

        return allData;
    }
}
