package com.wiley.cachedemo.cache.filesystem;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.cache.setup.BaseCache;
import com.wiley.cachedemo.cache.setup.Cache;
import com.wiley.cachedemo.cache.setup.LevelBasedCache;
import com.wiley.cachedemo.config.CacheConfig;
import com.wiley.cachedemo.config.CacheConfigsLoader;

import java.io.Serializable;
import java.util.List;

/**
 * File system cache class
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class FileSystemCache<K, V extends Serializable> extends LevelBasedCache<K, V> {

    private final BaseCache<K, V> cache;
    /**
     * cache storage key is the name of folder which stores cache
     */
    protected String cacheStorageKey;

    public FileSystemCache(int maxSize, EvictionStrategy evictionStrategy, Cache<K, V> context) {
        final CacheConfig fsCacheConfigs = CacheConfigsLoader.fileSystemCacheConfigs();

        int maxSizeFinal = fsCacheConfigs.getMaxSizeConfig().isUseForce() ? fsCacheConfigs.getMaxSizeConfig().getValue() : maxSize;
        EvictionStrategy evictionStrategyFinal = fsCacheConfigs.getEvictionStrategyConfig().isUseForce() ? fsCacheConfigs.getEvictionStrategyConfig().getValue() : evictionStrategy;

        cacheStorageKey = context.getClass().getName();

        if (evictionStrategyFinal == EvictionStrategy.LRU) {
            cache = new LRUCache<>(cacheStorageKey, maxSizeFinal);
        } else {
            cache = new LFUCache<>(cacheStorageKey, maxSizeFinal);
        }
    }

    @Override
    protected BaseCache<K, V> getCache() {
        return this.cache;
    }

    @Override
    public V get(Object key) {
        return cache.get(key);
    }

    @Override
    public V put(K key, V v) {
        return cache.put(key, v);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public V remove(K key) {
        return cache.remove(key);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public List<V> getAllData() {
        return cache.getAllData();
    }
}
