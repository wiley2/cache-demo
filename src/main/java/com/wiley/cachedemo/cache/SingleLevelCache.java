package com.wiley.cachedemo.cache;

import com.wiley.cachedemo.cache.enums.CacheType;
import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.cache.filesystem.FileSystemCache;
import com.wiley.cachedemo.cache.inmemory.InMemoryCache;
import com.wiley.cachedemo.cache.setup.BaseCache;
import com.wiley.cachedemo.cache.setup.Cache;

import java.io.Serializable;
import java.util.List;

/**
 * Single level cache
 * Can be use any cache type as the single level by giving {@link CacheType}
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public abstract class SingleLevelCache<K, V extends Serializable> implements Cache<K, V> {

    private final BaseCache<K, V> cache;

    protected SingleLevelCache(int maxSize, EvictionStrategy evictionStrategy, CacheType cacheType) {
        if (cacheType == CacheType.IN_MEMORY) {
            cache = new InMemoryCache<>(maxSize, evictionStrategy);
        } else {
            cache = new FileSystemCache<>(maxSize, evictionStrategy, this);
        }
    }

    @Override
    public V get(K key) {
        return cache.get(key);
    }

    @Override
    public void put(V v) {
        cache.put(getKey(v), v);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public void remove(K key) {
        cache.remove(key);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public List<V> getAllData() {
        return cache.getAllData();
    }
}
