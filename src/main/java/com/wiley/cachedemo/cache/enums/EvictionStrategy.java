package com.wiley.cachedemo.cache.enums;

/**
 * Enum for Eviction strategies
 */
public enum EvictionStrategy {
    LRU, LFU
}
