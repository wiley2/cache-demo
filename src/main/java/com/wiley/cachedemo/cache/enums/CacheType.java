package com.wiley.cachedemo.cache.enums;

/**
 * Enum for cache types
 */
public enum CacheType {
    IN_MEMORY, FILE_SYSTEM
}
