package com.wiley.cachedemo.cache.enums;

/**
 * Enum for Cache levels
 */
public enum CacheLevel {
    LEVEL_ONE, LEVEL_TWO
}
