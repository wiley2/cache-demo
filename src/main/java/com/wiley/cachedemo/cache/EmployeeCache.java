package com.wiley.cachedemo.cache;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.model.Employee;

/**
 * Cache to store {@link Employee} objects by it's ID
 */
public class EmployeeCache extends MultiLevelCache<Long, Employee> {

    private static EmployeeCache instance = new EmployeeCache();

    private EmployeeCache() {
        super(3, EvictionStrategy.LFU, 5, EvictionStrategy.LRU);
    }

    public static EmployeeCache getInstance() {
        return instance;
    }

    /**
     * Invalidate current cache object and re initiate cache with new config changes
     *
     * @return Newly created cache
     */
    public static EmployeeCache invalidateAndReInitiate() {
        instance = new EmployeeCache();
        return instance;
    }

    @Override
    public Long getKey(Employee employee) {
        return employee.getId();
    }
}
