package com.wiley.cachedemo.cache.inmemory;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import com.wiley.cachedemo.cache.setup.BaseCache;
import com.wiley.cachedemo.cache.setup.LevelBasedCache;
import com.wiley.cachedemo.config.CacheConfig;
import com.wiley.cachedemo.config.CacheConfigsLoader;

import java.util.List;

/**
 * In memory cache class
 *
 * @param <K>
 * @param <V>
 */
public class InMemoryCache<K, V> extends LevelBasedCache<K, V> {

    private final BaseCache<K, V> cache;

    public InMemoryCache(int maxSize, EvictionStrategy evictionStrategy) {
        final CacheConfig inMemCacheConfig = CacheConfigsLoader.inMemoryCacheConfigs();

        int maxSizeFinal = inMemCacheConfig.getMaxSizeConfig().isUseForce() ? inMemCacheConfig.getMaxSizeConfig().getValue() : maxSize;
        EvictionStrategy evictionStrategyFinal = inMemCacheConfig.getEvictionStrategyConfig().isUseForce() ? inMemCacheConfig.getEvictionStrategyConfig().getValue() : evictionStrategy;

        if (evictionStrategyFinal == EvictionStrategy.LRU) {
            cache = new LRUCacheMap<>(maxSizeFinal);
        } else {
            cache = new LFUCacheMap<>(maxSizeFinal);
        }
    }

    @Override
    protected BaseCache<K, V> getCache() {
        return this.cache;
    }

    @Override
    public V get(Object key) {
        return cache.get(key);
    }

    @Override
    public V put(K key, V v) {
        return cache.put(key, v);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public V remove(K key) {
        return cache.remove(key);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public List<V> getAllData() {
        return cache.getAllData();
    }
}