package com.wiley.cachedemo.cache.inmemory;

import com.wiley.cachedemo.cache.setup.BaseCache;

import java.util.*;

/**
 * In-memory cache implementation with eviction strategy of LFU
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class LFUCacheMap<K, V> extends LinkedHashMap<K, V> implements BaseCache<K, V> {

    /**
     * Max cache size
     */
    private final int maxSize;

    /**
     * Frequency map
     * This map will store the accessed frequency of each key
     */
    private final HashMap<Object, Integer> frequencyMap;

    /**
     * This queue will hold all previously accessed keys
     * so those can be used to determine which key should be evicted next from the cache
     */
    private final Queue<Object> evictionQueue;

    public LFUCacheMap(int maxSize) {
        this.maxSize = maxSize;

        frequencyMap = new HashMap<>();
        evictionQueue = new ArrayDeque<>();
    }

    @Override
    public V get(Object key) {
        V v = super.get(key);

        // if value is present in the cache need to update frequency map and eviction queue
        if (v != null) {
            evictionQueue.add(key);
            frequencyMap.merge(key, 1, Integer::sum);
        }

        return v;
    }

    @Override
    public V put(K key, V value) {

        if (!containsKey(key)) {

            // if cache size if equals to max size, need to evict an element to put the new element
            if (size() >= maxSize) {
                evictElement();
            }

            // update frequency map and eviction queue
            evictionQueue.add(key);
            Integer frequency = frequencyMap.get(key);
            if (frequency != null) {
                frequencyMap.put(key, frequency);
            } else {
                frequencyMap.put(key, 1);
            }
        }

        return super.put(key, value);
    }

    private void evictElement() {
        int minFreq = Integer.MAX_VALUE;
        final List<Object> keys = new ArrayList<>();

        // finding all keys with minimum frequencies
        for (Map.Entry<Object, Integer> frequencyEntry : frequencyMap.entrySet()) {
            if (frequencyEntry.getValue() < minFreq) {
                minFreq = frequencyEntry.getValue();
                keys.clear();
                keys.add(frequencyEntry.getKey());
            } else if (frequencyEntry.getValue() == minFreq) {
                keys.add(frequencyEntry.getKey());
            }
        }

        // if key count with minimum frequencies is 1,
        //      that means that key is the key which should be evicted
        // if key count is greater than 1
        //      need to find oldest element contains in eviction queue among keys list
        final Object keyToEvict;
        if (keys.size() == 1) {
            keyToEvict = keys.get(0);
        } else {
            keyToEvict = evictionQueue.stream().filter(keys::contains).findFirst().orElse(null);
        }

        // remove the key from all the places
        super.remove(keyToEvict);
        evictionQueue.remove(keyToEvict);
        frequencyMap.remove(keyToEvict);
    }

    @Override
    public void clear() {
        super.clear();
        evictionQueue.clear();
        frequencyMap.clear();
    }

    @Override
    public List<V> getAllData() {
        return new ArrayList<>(values());
    }

    @Override
    public V remove(Object key) {
        V v = super.remove(key);

        // remove relevant entry from frequency map too
        frequencyMap.remove(key);

        // remove all present elements of the given key in eviction queue
        evictionQueue.removeIf(o -> o.equals(key));
        return v;
    }
}
