package com.wiley.cachedemo.cache.inmemory;

import com.wiley.cachedemo.cache.setup.BaseCache;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * In-memory cache implementation with eviction strategy of LRU
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public class LRUCacheMap<K, V> extends LinkedHashMap<K, V> implements BaseCache<K, V> {

    /**
     * Max cache size
     */
    private final int maxSize;

    public LRUCacheMap(int maxSize) {
        // accessOrder = true means all values are ordered according to accessed order
        // that means, after accessing an element, it will be moved to the end of the collection
        super(maxSize, 0.75f, true);
        this.maxSize = maxSize;
    }

    /**
     * Returning true will be removed eldest entry in the collection
     *
     * @param eldest currently eldest entry
     * @return Boolean value to determine that the eldest value should be removed or not
     */
    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return maxSize > 0 && size() > maxSize;
    }

    @Override
    public List<V> getAllData() {
        return new ArrayList<>(values());
    }
}
