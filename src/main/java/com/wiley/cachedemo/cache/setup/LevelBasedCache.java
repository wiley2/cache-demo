package com.wiley.cachedemo.cache.setup;

import com.wiley.cachedemo.cache.enums.CacheLevel;

import java.util.List;

/**
 * Extending this class will give the ability to work as a multi level cache
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public abstract class LevelBasedCache<K, V> implements BaseCache<K, V> {

    // Next level cache
    private LevelBasedCache<K, V> nextCache;

    // Previous level cache
    private LevelBasedCache<K, V> previousCache;

    // Cache level
    private CacheLevel level;

    protected abstract BaseCache<K, V> getCache();

    /**
     * In this method it checked each and every linked caches for the given key
     * If the key is found in a any level it will return and will update all the previous levels too
     *
     * @param key Given key
     * @return Value
     */
    public V getByLevel(Object key) {
        V value = getCache().get(key);

        // If the value is found in this level, it will be returned to the above level
        if (value != null) {
            return value;
        }

        // If not found, checking whether a next level cache is available or not
        if (nextCache != null) {
            // If a next level cache is available,
            // checking whether the value for given key is exists or not in next level
            value = nextCache.getByLevel(key);

            // If value for the key is exists there, updating previous levels too
            if (value != null) {
                if (previousCache != null) {
                    previousCache.putByLevel((K) key, value);
                }
            }
        }

        return value;
    }

    /**
     * Putting value to all linked cache levels
     *
     * @param key Key
     * @param v   value
     */
    public void putByLevel(K key, V v) {
        getCache().put(key, v);

        if (nextCache != null) {
            nextCache.put(key, v);
        }
    }

    /**
     * Clearing caches in all levels
     */
    public void clearByLevel() {
        getCache().clear();
        if (nextCache != null) {
            nextCache.clear();
        }
    }

    /**
     * Remove a cache key and value from all levels
     *
     * @param key
     */
    public void removeByLevel(K key) {
        getCache().remove(key);
        if (nextCache != null) {
            nextCache.remove(key);
        }
    }

    /**
     * Retrieving cache size by cache level
     *
     * @param level Cache level
     * @return Cache size according to the level
     */
    public int sizeByLevel(CacheLevel level) {
        if (level == this.level) {
            return getCache().size();
        }

        return nextCache.sizeByLevel(level);
    }

    /**
     * Getting all currently available data in cache according to the given cache level
     *
     * @param level Cache level
     * @return Cached data
     */
    public List<V> getAllDataByLevel(CacheLevel level) {
        if (level == this.level) {
            return getCache().getAllData();
        }

        return nextCache.getAllDataByLevel(level);
    }

    public void setNextLevelCache(LevelBasedCache<K, V> nextLevelCache) {
        this.nextCache = nextLevelCache;
    }

    public void setPreviousLevelCache(LevelBasedCache<K, V> previousLevelCache) {
        this.previousCache = previousLevelCache;
    }

    public void setLevel(CacheLevel level) {
        this.level = level;
    }
}
