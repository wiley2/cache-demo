package com.wiley.cachedemo.cache.setup;

import java.util.List;

/**
 * Interface of Cache
 * This interface is implemented by top level cache handlers
 * Method under this interface will be accessed by outside
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface Cache<K, V> {

    /**
     * Get cached value by key
     *
     * @param key Key
     * @return Value related to the given key
     */
    V get(K key);

    /**
     * Retrieve key according to given value
     *
     * @param v Value
     * @return Key
     */
    K getKey(V v);

    /**
     * Put value to the cache
     *
     * @param v Value
     * @return Added value
     */
    void put(V v);

    /**
     * Clear all cache items
     */
    void clear();

    /**
     * Remove a cached value by it's key
     *
     * @param key Key
     * @return Removed value
     */
    void remove(K key);

    /**
     * Getting cache size
     *
     * @return Cache size
     */
    int size();

    /**
     * Getting all currently available data in cache
     *
     * @return Cached data
     */
    List<V> getAllData();
}
