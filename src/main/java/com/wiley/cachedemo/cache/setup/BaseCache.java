package com.wiley.cachedemo.cache.setup;

import java.util.List;

/**
 * Base Cache
 * This interface is implemented by bottom level cache handlers
 *
 * @param <K> Key type
 * @param <V> Value type
 */
public interface BaseCache<K, V> {

    /**
     * Get cached value by key
     *
     * @param key Key
     * @return Value related to the given key
     */
    V get(Object key);

    /**
     * Put value to the cache according to the key
     *
     * @param key Key
     * @param v   Value
     * @return Added value
     */
    V put(K key, V v);

    /**
     * Remove a cached value by it's key
     *
     * @param key Key
     * @return Removed value
     */
    V remove(K key);

    /**
     * Clear all cache items
     */
    void clear();

    /**
     * Current size of the cache
     *
     * @return Size of the cache
     */
    int size();

    /**
     * Getting all currently available data in cache
     *
     * @return All currently available data in cache
     */
    List<V> getAllData();
}
