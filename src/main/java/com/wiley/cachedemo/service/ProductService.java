package com.wiley.cachedemo.service;

import com.wiley.cachedemo.cache.ProductCache;
import com.wiley.cachedemo.model.Product;
import com.wiley.cachedemo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service class for Product related operations
 */
@Service
public class ProductService {

    private final Logger logger = Logger.getLogger("PRODUCT SERVICE");

    private final ProductRepository productRepository;
    private final ProductCache productCache = ProductCache.getInstance();

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Retrieve product by ID
     *
     * @param id Product ID
     * @return Detailed object of product if present, otherwise null.
     */
    public Product getById(long id) {

        // checking whether product with given id is exists in cache or not
        Product product = productCache.get(id);

        if (product != null) {
            logger.log(Level.INFO, "[" + id + "]" + " Retrieved from cache");
            return product;
        }

        // if not, retrieving from DB
        product = productRepository.getOne(id);
        logger.log(Level.INFO, "[" + id + "]" + " Retrieved from DB");

        // then added to cache too
        productCache.put(product);

        return product;
    }
}
