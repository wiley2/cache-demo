package com.wiley.cachedemo.service;

import com.wiley.cachedemo.cache.EmployeeCache;
import com.wiley.cachedemo.model.Employee;
import com.wiley.cachedemo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Service class for employee related operations
 */
@Service
public class EmployeeService {

    private final Logger logger = Logger.getLogger("EMPLOYEE SERVICE");

    private final EmployeeRepository employeeRepository;
    private final EmployeeCache employeeCache = EmployeeCache.getInstance();

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    /**
     * Retrieve employee by ID
     *
     * @param id Employee ID
     * @return Detailed object of employee if present, otherwise null.
     */
    public Employee getById(long id) {

        // checking whether employee with given id is exists in cache or not
        Employee employee = employeeCache.get(id);

        if (employee != null) {
            logger.log(Level.INFO, "[" + id + "]" + " Retrieved from cache");
            return employee;
        }

        // if not, retrieving from DB
        employee = employeeRepository.getOne(id);
        logger.log(Level.INFO, "[" + id + "]" + " Retrieved from DB");

        // then added to cache too
        employeeCache.put(employee);

        return employee;
    }
}
