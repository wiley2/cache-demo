package com.wiley.cachedemo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Entity class of Employee
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee implements Serializable {

    /**
     * Unique ID
     */
    @Id
    private long id;

    private String firstName;
    private String lastName;
    private int YearsOfExperience;
}
