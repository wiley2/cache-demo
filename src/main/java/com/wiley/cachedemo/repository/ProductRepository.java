package com.wiley.cachedemo.repository;

import com.wiley.cachedemo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link com.wiley.cachedemo.model.Product}
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
