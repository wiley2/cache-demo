package com.wiley.cachedemo.repository;

import com.wiley.cachedemo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for {@link Employee}
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
