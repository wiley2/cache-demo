package com.wiley.cachedemo.config;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Config loader class for cache configurations
 * This will load configs from a config file when app starting up
 */
public class CacheConfigsLoader {

    /**
     * Config file
     * This file will be located under resources directory
     */
    private static final String CONFIG_FILE_NAME = "application.properties";

    /**
     * All loaded configs
     */
    private static final Properties configs;

    /**
     * Mapped configs
     */
    private static CacheConfig inMemoryCacheConfig;
    private static CacheConfig fileSystemCacheConfig;

    /*
      Static block to initialize configs
     */
    static {
        configs = loadConfigs();
        setConfigs();
    }

    private CacheConfigsLoader() {
    }

    /**
     * Load configs from the given location
     *
     * @return Loaded configs
     */
    public static Properties loadConfigs() {
        Properties configuration = new Properties();
        InputStream inputStream = CacheConfigsLoader.class.getClassLoader().getResourceAsStream(CONFIG_FILE_NAME);
        try {
            configuration.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return configuration;
    }

    /**
     * Map loaded configs from {@link #configs} to {@link #inMemoryCacheConfig} and {@link #fileSystemCacheConfig}
     */
    private static void setConfigs() {
        inMemoryCacheConfig = createConfigByType("in-memory");
        fileSystemCacheConfig = createConfigByType("file-system");
    }

    /**
     * Helping method to map configs
     *
     * @param type Config type
     *             Value should be a one of 'in-memory' or 'file-system'
     * @return Mapped config object
     */
    private static CacheConfig createConfigByType(String type) {
        String replacement = "cache." + type;
        String maxSize = "{0}.max-size";
        String maxSizeUseForce = "{0}.max-size.use-force";
        String evictionStrategy = "{0}.eviction-strategy";
        String evictionStrategyUseForce = "{0}.eviction-strategy.use-force";

        CacheConfigItem<Integer> maxSizeConfig = new CacheConfigItem<>(
                getInt(maxSize.replace("{0}", replacement)),
                getBoolean(maxSizeUseForce.replace("{0}", replacement))
        );

        CacheConfigItem<EvictionStrategy> evictionStrategyConfig = new CacheConfigItem<>(
                EvictionStrategy.valueOf(getString(evictionStrategy.replace("{0}", replacement)).toUpperCase()),
                getBoolean(evictionStrategyUseForce.replace("{0}", replacement))
        );

        return new CacheConfig(maxSizeConfig, evictionStrategyConfig);
    }

    public static String getString(String key) {
        return configs.getProperty(key);
    }

    public static int getInt(String key) {
        return Integer.parseInt(getString(key));
    }

    public static boolean getBoolean(String key) {
        return Boolean.parseBoolean(getString(key));
    }

    public static CacheConfig inMemoryCacheConfigs() {
        return inMemoryCacheConfig;
    }

    public static CacheConfig fileSystemCacheConfigs() {
        return fileSystemCacheConfig;
    }
}
