package com.wiley.cachedemo.config;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Model class for Cache config items
 *
 * @param <T>
 */
@Data
@AllArgsConstructor
public class CacheConfigItem<T> {

    private T value;
    private boolean useForce;
}
