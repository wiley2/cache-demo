package com.wiley.cachedemo.config;

import com.wiley.cachedemo.cache.enums.EvictionStrategy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Model class for cache configs
 */

@Getter
@Setter
@AllArgsConstructor
public class CacheConfig {

    private CacheConfigItem<Integer> maxSizeConfig;
    private CacheConfigItem<EvictionStrategy> evictionStrategyConfig;
}
